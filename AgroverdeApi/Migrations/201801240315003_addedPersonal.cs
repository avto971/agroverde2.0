namespace AgroverdeApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedPersonal : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Personal_Information", "Email", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Personal_Information", "Email");
        }
    }
}
