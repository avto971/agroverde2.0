namespace AgroverdeApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Comment = c.String(maxLength: 144, unicode: false),
                        Product_Sale = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Measurement",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Detail = c.String(maxLength: 144, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Municipio",
                c => new
                    {
                        Municipio_id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 45, unicode: false),
                        Provincia_id = c.Int(),
                    })
                .PrimaryKey(t => t.Municipio_id);
            
            CreateTable(
                "dbo.Personal_Information",
                c => new
                    {
                        Identification = c.String(nullable: false, maxLength: 13, unicode: false),
                        Name = c.String(maxLength: 40, unicode: false),
                        LastName = c.String(maxLength: 40, unicode: false),
                        Municipio = c.Int(),
                        UserId = c.String(nullable: false, maxLength: 128),
                        PhoneNumber = c.String(maxLength: 13, unicode: false),
                    })
                .PrimaryKey(t => t.Identification);
            
            CreateTable(
                "dbo.Phone",
                c => new
                    {
                        PhoneNum = c.String(nullable: false, maxLength: 12, unicode: false),
                        Cedula = c.String(nullable: false, maxLength: 13, unicode: false),
                    })
                .PrimaryKey(t => t.PhoneNum);
            
            CreateTable(
                "dbo.Product_Category",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category = c.String(maxLength: 45, unicode: false),
                        ImageCategory = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Product_Request",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Product_id = c.Int(),
                        Requester = c.String(maxLength: 128),
                        Measurement_id = c.Int(),
                        Quantity = c.Int(),
                        Date = c.DateTime(),
                        Details = c.String(maxLength: 800, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Measurement", t => t.Measurement_id)
                .ForeignKey("dbo.Product", t => t.Product_id)
                .Index(t => t.Product_id)
                .Index(t => t.Measurement_id);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 45, unicode: false),
                        CategoryId = c.Int(),
                        Photo = c.String(maxLength: 500, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Product_Sale",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Product_id = c.Int(),
                        Owners = c.String(maxLength: 128),
                        Measurement_id = c.Int(),
                        Quantity = c.Int(),
                        Price = c.Int(),
                        Details = c.String(maxLength: 800),
                        Date = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Measurement", t => t.Measurement_id)
                .ForeignKey("dbo.Product", t => t.Product_id)
                .Index(t => t.Product_id)
                .Index(t => t.Measurement_id);
            
            CreateTable(
                "dbo.Provincia",
                c => new
                    {
                        Provincia_id = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 45, unicode: false),
                    })
                .PrimaryKey(t => t.Provincia_id);
            
            CreateTable(
                "dbo.Rol",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Description = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.RolUsuario",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Rol", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Usuario", t => t.IdentityUser_Id)
                .Index(t => t.RoleId)
                .Index(t => t.IdentityUser_Id);
            
            CreateTable(
                "dbo.User_Prod_Cat",
                c => new
                    {
                        UserID = c.String(nullable: false, maxLength: 128),
                        CategoryId = c.Int(nullable: false),
                        Product_Category_Id = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserID, t.CategoryId })
                .ForeignKey("dbo.Product_Category", t => t.Product_Category_Id)
                .Index(t => t.Product_Category_Id);
            
            CreateTable(
                "dbo.Usuario",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Usuario", t => t.IdentityUser_Id)
                .Index(t => t.IdentityUser_Id);
            
            CreateTable(
                "dbo.UsuarioLogeado",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Usuario", t => t.IdentityUser_Id)
                .Index(t => t.IdentityUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RolUsuario", "IdentityUser_Id", "dbo.Usuario");
            DropForeignKey("dbo.UsuarioLogeado", "IdentityUser_Id", "dbo.Usuario");
            DropForeignKey("dbo.AspNetUserClaims", "IdentityUser_Id", "dbo.Usuario");
            DropForeignKey("dbo.User_Prod_Cat", "Product_Category_Id", "dbo.Product_Category");
            DropForeignKey("dbo.RolUsuario", "RoleId", "dbo.Rol");
            DropForeignKey("dbo.Product_Sale", "Product_id", "dbo.Product");
            DropForeignKey("dbo.Product_Sale", "Measurement_id", "dbo.Measurement");
            DropForeignKey("dbo.Product_Request", "Product_id", "dbo.Product");
            DropForeignKey("dbo.Product_Request", "Measurement_id", "dbo.Measurement");
            DropIndex("dbo.UsuarioLogeado", new[] { "IdentityUser_Id" });
            DropIndex("dbo.AspNetUserClaims", new[] { "IdentityUser_Id" });
            DropIndex("dbo.Usuario", "UserNameIndex");
            DropIndex("dbo.User_Prod_Cat", new[] { "Product_Category_Id" });
            DropIndex("dbo.RolUsuario", new[] { "IdentityUser_Id" });
            DropIndex("dbo.RolUsuario", new[] { "RoleId" });
            DropIndex("dbo.Rol", "RoleNameIndex");
            DropIndex("dbo.Product_Sale", new[] { "Measurement_id" });
            DropIndex("dbo.Product_Sale", new[] { "Product_id" });
            DropIndex("dbo.Product_Request", new[] { "Measurement_id" });
            DropIndex("dbo.Product_Request", new[] { "Product_id" });
            DropTable("dbo.UsuarioLogeado");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.Usuario");
            DropTable("dbo.User_Prod_Cat");
            DropTable("dbo.RolUsuario");
            DropTable("dbo.Rol");
            DropTable("dbo.Provincia");
            DropTable("dbo.Product_Sale");
            DropTable("dbo.Product");
            DropTable("dbo.Product_Request");
            DropTable("dbo.Product_Category");
            DropTable("dbo.Phone");
            DropTable("dbo.Personal_Information");
            DropTable("dbo.Municipio");
            DropTable("dbo.Measurement");
            DropTable("dbo.Comment");
        }
    }
}
