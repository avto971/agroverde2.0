angular.
    module('agroverde').
    controller('logincontroller', function ($scope, authService, $http) {
        $scope.loginError = '';
        $scope.categories = [];


        $scope.login = function () {
            authService.authenticate($scope.username, $scope.password, function (err) {
                if (err.error) {
                    $scope.loginError = 'Usuario o contraseña incorrectos';
                } else {
                    $scope.loginError = 'Revise su conexión a internet!';
                }
            });
        }

        $scope.getCategories = function () {
            $http.get('api/Product_Category', {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.categories = res;
                console.log(res);
            }).error(function (err) {
                throw err;
            });
        }

        $scope.getCategories();

    });