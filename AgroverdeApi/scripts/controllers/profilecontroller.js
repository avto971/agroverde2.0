﻿angular.
    module('agroverde').
    controller('profilecontroller', function ($scope, $http, $stateParams, $state) {

        function getProfileInfo() {
            var _userid = JSON.parse(
                localStorage.getItem('user_info')).User_id;

            if (!_userid) $state.go('login');



            var _token = localStorage.getItem('token');
            var auth_header = { 'Authorization': 'Bearer ' + _token };

            $http.get('api/Account/GetUserInfoById/?id=' + _userid, {
                    headers: auth_header
                })
                .success(function (res) {
                    $scope.user = res;
                }).error(function (err) {
                    throw err;
                });
        }

        getProfileInfo();
    });