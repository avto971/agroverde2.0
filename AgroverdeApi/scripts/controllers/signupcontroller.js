angular.
    module('agroverde').
    controller('sign-upController', function ($scope, $http, authService) {

        $scope.provincias = [];
        $scope.municipios = [];
        $scope.errors = [];

        function getProvincias() {
            $http.get('api/provincias', {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.provincias = res;
            }).error(function (err) {
                throw err;
            });
        }

        getProvincias();

        $scope.getMunicipios = function (id) {
            $http.get('api/municipios/' + id, {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.municipios = res;
            }).error(function (err) {
                throw err;
            });
        }

        function handleErrors(res) {
           // if (res.ModelState['model.Identification']) {
           //     //$scope.errors.push("Cédula no válida! Asegúrese de poner los guiones");
           // }
            //else  
            if (res.ModelState['model.Email']) {
                $scope.errors.push("Email no válido o ya registrado!");
            }
            else if (res.ModelState['model.Password']) {
                $scope.errors.push("Contraseña no válida! Debe contener al menos 6 caracteres y un número");
            }
            else if (res.ExceptionMessage) {
                $scope.errors.push("Por favor compruebe su conexión a internet!");
            } else if (res.ModelState || res.ModelState[""].length){
                for (var i = 0; i < res.ModelState[""].length; i++) {
                    var d = res.ModelState[""][i];
                    $scope.errors.push(d);
                }
            }

        }

        $scope.register = function () {
            $scope.errors = [];

            if (!$scope.name || !$scope.role || !$scope.lastName || !$scope.email || !$scope.municipio.Municipio_id || !$scope.phoneNumber) {
                $scope.errors.push("Completar todos los campos!!");
                return;
            }

            var data = {
                Name: $scope.name,
                LastName: $scope.lastName,
                Email: $scope.email,
                //Identification: $scope.identification,
                Municipio: $scope.municipio.Municipio_id,
                Password: $scope.password,           
                ConfirmPassword: $scope.password,
                PhoneNumber: $scope.phoneNumber,
                Role: $scope.role
                


            };

            console.log(data);

            $http.post('api/Account/Register', data).success(function (res) {
                console.log(res);
                authService.authenticate(data.Email, data.Password);
            }).error(function (err) {
                handleErrors(err);
            });
        }
    });
