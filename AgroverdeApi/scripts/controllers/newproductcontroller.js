angular.
    module('agroverde').
    controller('newProductController', function ($scope, $http, $state) {

        $scope.product = {
            Photo: null,
           
        };
        $scope.PriceDetail = [{
            MeasurementId: null,
            Price: 0
        }];
        $scope.categories = [];
        $scope.products = [];
        $scope.measurements = [];

        $scope.addPriceDetail = function() {
            $scope.PriceDetail.push({
                MeasuramentId: null,
                Price: 0
            });
        };
        $scope.deletePriceDetail = function (index) {
            $scope.PriceDetail.splice(index, 1);
        };


        $(document).on("click", "#imgProfile, #imgProfile ~ i.fa-camera, #cambiarFoto", (ev) => {
            $("#InputFileImage").click();
        });

        $(document).on("change", "#InputFileImage", function (ev) {
            var img = ev.target;
            $scope.product.Photo = null;

            let reader = new FileReader();
            reader.onload = function (e) {
                $scope.product.Photo = e.target.result;
                $scope.$apply();
            };

            reader.readAsDataURL(img.files[0]);
        })
           
 

        function getCategories() {
            $http.get('api/Product_Category', {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.categories = res;
            }).error(function (err) {
                throw err;
            });
        }

        function getMeasurements() {
            $http.get('api/measurements', {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.measurements = res;
            }).error(function (err) {
                throw err;
            });
        }

        $scope.getProducts = function (category) {
            $http.get('api/products/' + category, {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.products = res;
            }).error(function (err) {
                throw err;
            });
        }

        $scope.saveProduct = function () {
            var data = {
                Photo: $scope.product.Photo,
                Product_id: $scope.product.Id,
                Date: new Date(),
                Details: $scope.details,
                Date: new Date(),
                Product: null,
                ProductSaleMeasuramentDetail: $scope.PriceDetail,
                Owners: JSON.parse(
                    localStorage.getItem('user_info')
                ).User_id
            };
            console.log(data);
            $http.post('api/Product_Sale', data)
                .success(function (res) {
                    $state.go('farmer.products');
                }).error(function (err) {
                    throw err;
                })
        }

        getCategories();
        getMeasurements();

    });

