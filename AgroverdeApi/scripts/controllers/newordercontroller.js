angular.
    module('agroverde').
    controller('newordercontroller', function ($scope, $http, $state) {

        $scope.categories = [];
        $scope.products = [];
        $scope.measurements = [];

        function getCategories() {
            $http.get('api/Product_Category', {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.categories = res;
            }).error(function (err) {
                throw err;
            });
        }

        function getMeasurements() {
            $http.get('api/measurements', {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.measurements = res;
            }).error(function (err) {
                throw err;
            });
        }

        $scope.getProducts = function (category) {
            $http.get('api/products/' + category, {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.products = res;
            }).error(function (err) {
                throw err;
            });
        }

        $scope.saveProduct = function () {
            var data = {
                Product_id: $scope.product.Id,
                Measurement_id: $scope.measurement.Id,
                Quantity: $scope.quantity,
                Details: $scope.details,
                Date: null,
                Measurement: null,
                Product: null,
                Requester: JSON.parse(
                    localStorage.getItem('user_info')
                ).User_id
            };
            console.log(data);
            $http.post('api/Product_Request', data)
                .success(function (res) {
                    console.log(res);
                    $state.go('merchant.orders');
                }).error(function (err) {
                    throw err;
                })
        }

        getCategories();
        getMeasurements();

    });

