﻿angular.
    module('agroverde').
    controller('productDetailResultCtrl', function ($scope, $http, $stateParams, $state) {


        $scope.productSalesQuotesDetails = [];
        $scope.measurements = [];
        $scope.measurements.push({description:'Libra', id:1})

        $scope.productId = $stateParams.pId;

        $scope.productsResultDetail = {};
        $scope.measurementPricePer = 0;
        $scope.selectedMeasurement = null;
        $scope.productSaleMeasurementDetails = [];
        $scope.availableMeasurements = [];


        $scope.getProductDetail = function () {
            $http.get('api/ProductSale/GetProductSaleDetail/' + $scope.productId, {
                'Content-type': 'application/json'
            }).success(function (res) {

                console.log(res)
                $scope.productsResultDetail = res;
                $scope.loadProductSaleMeasurementDetails(res.Product_Sale.Id);
            }).error(function (err, errorCode) {

                throw err;


            });
        }

        $scope.loadProductSaleMeasurementDetails = function (productSaleId) {
            $http.get('api/product_sale/GetProduct_SaleMeasurementDetails/' + productSaleId, {
                'Content-type': 'application/json'
            }).success(function (res) {
                console.log(res)
                $scope.productSaleMeasurementDetails = res;

                for (var i = 0; i < res.length; i++) {
                    $scope.availableMeasurements.push(res[i].Measurement);
                }


            }).error(function (err, errorCode) {
                throw err;
            });
        }

        $scope.getProductDetail();

        $scope.addProductSaleQuoteDetail = function () {
            $scope.productSalesQuotesDetails.push(new productSalesQuotesDetail());
        }

        $scope.deleteProductSaleQuoteDetail = function (index) {
            $scope.productSalesQuotesDetails.splice(index, 1);
        }

        $scope.measurementChange = function (o, productSaleQuoteDetail) {

            var detail = $scope.productsResultDetail.Product_Sale.ProductSaleMeasuramentDetail.filter(function (MeasuramentDetail) {

                return MeasuramentDetail.Measurement.Id == o
            })[0];

            productSaleQuoteDetail.MeasurementId = o;

            productSaleQuoteDetail.measurementPrice = detail.Price;
        }

        $scope.quantityChange = function (productSaleQuoteDetail) {
            if (productSaleQuoteDetail.quantity) {
                productSaleQuoteDetail.total = productSaleQuoteDetail.quantity * productSaleQuoteDetail.measurementPrice;
            }
        }

        $scope.sendProductSaleQuote = function () {

            var isAuth = JSON.parse(localStorage.getItem('token'));

            if (!isAuth) {
                return swal({
                    title: 'Nota',
                    text: "No puede realizar una cotización sin registrarse en el sistema",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Registrame',
                    cancelButtonText: 'No registrarme',
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false,
                    reverseButtons: true
                }).then((result) => {
                   if (result.value) {
                       $state.go('login');
                       window.location.href = "#/login"
                       window.location.reload();
                   } else if (result.dismiss === swal.DismissReason.cancel) {
                       swal.close();
                   }
               })
            }



            var productSaleQuote = {
                Id: 0,
                ProductSaleId : $scope.productsResultDetail.Product_Sale.Id,
                MerchantId: $scope.productsResultDetail.Product_Sale.Owners,
                FarmerId: $scope.productsResultDetail.Product_Sale.Owners,
                QuoteDate: new Date(),
                QuoteStatusDateChange: new Date(),
                QuoteStatus: 3,
                ProductSalesQuotesDetails: $scope.productSalesQuotesDetails
            }

            $http.post('api/ProductSalesQuotes', productSaleQuote)
                .success(function (res) {
                    //console.log(res);
                   // $state.go('merchant.orders');
                }).error(function (err) {
                    throw err;
                })
        }

    });

var productSalesQuotesDetail = function () {
    this.Id = 0;
    this.productSalesQuoteId = 0;
    this.measurementId = 0;
    this.quantity = 0;
    this.measurementPrice = 0;
    this.total = 0;

}