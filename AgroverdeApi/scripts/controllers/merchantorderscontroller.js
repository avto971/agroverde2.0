angular.
	module('agroverde').
	controller('merchantorderscontroller', function($scope, $http){

        var _userid = JSON.parse(
            localStorage.getItem('user_info')).User_id;

        $scope.quotes = [];
        $scope.currentFarmerPersonalInformation = null;
        $scope.quoteSelected = null;

        function getQuotes() {
            $http.get('api/ProductSalesQuotes/GetQuotes?merchantId=' + _userid, {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.quotes = res;
            }).error(function (err) {
                throw err;
            });
        }

        $scope.getQuoteInformation = function (quote) {
            $scope.getFarmerPersonalInformation(quote);
            $scope.quoteSelected = quote;
        }

        $scope.sendMessageToMerchant = function () {
            if ($scope.CommentWritten == null) {
                alert("Escriba un comentario")
            }
            var data = {
                Id: 0,
                ProductQuoteSaleId: $scope.quoteSelected.Id,
                FarmerId: $scope.quoteSelected.FarmerId,
                MerchantId: $scope.quoteSelected.MerchantId,
                Comment: $scope.CommentWritten,
                IsMerchantComment: true,
                CommentDate: new Date()
            };
            console.log(data);
            $http.post('api/ProductSalesQuotes/SaveComment', data)
                .success(function (res) {
                    console.log(res);
                    $scope.quoteSelected.FarmerMerchantComments.push(res);
                    $scope.CommentWritten = null;
                    //$state.go('farmer');
                }).error(function (err) {
                    throw err;
                })
        }

        $scope.getFarmerPersonalInformation = function (quote) {
            $http.get('api/PersonalInformation/GetPersonalInformationFromUser/' + quote.FarmerId, {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.currentFarmerPersonalInformation = res;
            }).error(function (err) {
                throw err;
            });


        }

        getQuotes();


	});