angular.
    module('agroverde').
    controller('offersformerchantcontroller', function ($scope, $http) {

        var _userid = JSON.parse(
            localStorage.getItem('user_info')).User_id;

        $scope.orders = [];

        function getOrders() {
            $http.get('api/Product_Sale/', {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.orders = res;
            }).error(function (err) {
                throw err;
            });
        }

        getOrders();

    });