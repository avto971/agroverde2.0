angular.
    module('agroverde').
    controller('farmerproductscontroller', function ($scope, $http) {

        var _userid = JSON.parse(
            localStorage.getItem('user_info')).User_id;

        $scope.productsales = [];

        function getProducts() {
            $http.get('api/product_sale/userid/' + _userid, {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.productsales = res;
            }).error(function (err) {
                throw err;
            });
        }


        $scope.updateStatusPublish = function updateStatusPublish(productsale) {

            productsale.Publish = !productsale.Publish;
            $http.post('api/product_sale/updateProductSale', productsale)
                .success(function (res) {
                }).error(function (err) {
                    throw err;
            });
        }

        getProducts();

    });