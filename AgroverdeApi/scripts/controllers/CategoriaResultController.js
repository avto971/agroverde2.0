﻿angular.
    module('agroverde').
    controller('categoriaResultCtrl', function ($scope, $http, $stateParams) {

        $scope.category = "Dolor";
        $scope.categoryId = $stateParams.cId;
        $scope.productsResults = [];
        $scope.categories = [];
         

        $scope.getProductByCategory = function () {
            $http.get('api/Products/FilterProductByCategoryId/' + $scope.categoryId, {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.noResult = true
                $scope.productsResults = res;
                }).error(function (err, errorCode) {
                    console.log();
                    if (errorCode == 404) {
                        $scope.productsResults = [];
                        return
                    }

                throw err;


            });
        }
        $scope.getCategoryById = function () {
            $http.get('api/ProductCategory/GetProduct_Category/' + $scope.categoryId, {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.category = res;
            }).error(function (err) {
                throw err;
                console.log(err);

            });
        }   

        $scope.getCategories = function () {
            $http.get('api/Product_Category', {
                'Content-type': 'application/json'
            }).success(function (res) {
                $scope.categories = res;
                console.log(res);
            }).error(function (err) {
                throw err;
                console.log(err);
            });
        }

        $scope.selectCategory = function (category, index) {
            $scope.category = category;
            $scope.categoryId = category.Id;
            var items = document.querySelectorAll(".list-group-item-action");
            items.forEach(function (value) {
                var aitem = angular.element(value);
                aitem.removeClass('active')
            })
            var item = document.querySelector("a#item-" + index);
            var aitem = angular.element(item);
            aitem.addClass("active");
            $scope.getProductByCategory();
        }

        $scope.getProductByCategory();
        $scope.getCategoryById();
        $scope.getCategories();

    });