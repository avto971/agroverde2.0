angular.
    module('agroverde',
    ['ui.router',
        'uiRouterStyles',
        'angular-loading-bar',
        'oc.lazyLoad']).

    config(function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
        });


        $urlRouterProvider.otherwise('login');

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'partials/login.html',
                controller: 'logincontroller',
                data: {
                    css: ['css/login.css']
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/logincontroller.js']
                        });
                    }
                }
            })

            .state('nosotros', {
                url: '/nosotros',
                templateUrl: 'partials/nosotros.html',
                data: {
                    css: ['css/nosotros.css']
                }
            })

            .state('contactanos', {
                url: '/contactanos',
                templateUrl: 'partials/contactanos.html',
                controller: 'contactanosCtrl',
                data: {
                    css: ['css/contactanos.css']

                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/contactanoscontroller.js']
                        });
                    }
                }
            })

            .state('ProductoResultado', {
                url: '/ProductoResultado?cId',
                templateUrl: 'partials/categoriaResult.html',
                controller: 'categoriaResultCtrl',
                data: {
                    css: ['css/login.css']
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/categoriaResultController.js']
                        });
                    }
                }
            })


            .state('ProductDetailResult', {
                url: '/productDetail?pId',
                templateUrl: 'partials/ProductDetailResult.html',
                controller: 'productDetailResultCtrl',
                data: {
                    css: ['css/login.css']
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/productDetailResultController.js']
                        });
                    }
                }
            })

            .state('searchResult', {
                url: '/busqueda',
                templateUrl: 'search.html',
                data: {
                    css: ['css/login.css']
                }
            })

            .state('logout', {
                url: '/logout',
                controller: function (authService) {
                    authService.logOut();
                }
            })

            .state('sign-up', {
                url: '/sign-up',
                templateUrl: 'partials/signup.html',
                controller: 'sign-upController',
                data: {
                    css: ['css/registro.css']
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/signupcontroller.js']
                        });
                    }
                }
            })

          

            .state('farmer', {
                url: '/farmer',
                templateUrl: 'partials/farmer/farmer.html',
                data: {
                    css: ['css/estilo.css'],
                    requireAuthorization: true
                },
                redirectTo: 'farmer.offers'
            })

            .state('farmer.offers', {
                url: '/offers',
                templateUrl: 'partials/farmer/offers.html',
                controller: 'ordersforfarmers',
                data: {
                    requireAuthorization: true
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/ordersforfarmers.js']
                        });
                    }
                }
            })

            .state('farmer.searchResult', {
                url: '/busqueda',
                templateUrl: 'partials/farmer/search.html',
                data: {
                    css: ['css/estilo.css'],
                    requireAuthorization: true
                }
            })


            .state('farmer.categoria', {
                url: '/categoria',
                templateUrl: 'partials/farmer/categoria.html',
                data: {
                    css: ['css/estilo.css'],
                    requireAuthorization: true
                }
            })


            .state('farmer.categoriaResult', {
                url: '/categoriaResult',
                templateUrl: 'partials/farmer/categoriaResult.html',
                controller: 'productscontroller',
                data: {
                    requireAuthorization: true
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/productscontroller.js']
                        });
                    }
                }
            })


            .state('farmer.newproduct', {
                url: '/newproduct',
                templateUrl: 'partials/farmer/newproduct.html',
                controller: 'newProductController',
                data: {
                    requireAuthorization: true
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/newproductcontroller.js']
                        });
                    }
                }
            })

            .state('farmer.products', {
                url: '/products',
                templateUrl: 'partials/farmer/products.html',
                controller: 'farmerproductscontroller',
                data: {
                    requireAuthorization: true
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/farmerproductscontroller.js']
                        });
                    }
                }
            })

            .state('farmer.nosotros', {
                url: '/nosotros',
                templateUrl: 'partials/farmer/nosotros.html',
                data: {
                    css: ['css/nosotros.css']
                }
            })

            .state('farmer.contactanos', {
                url: '/contactanos',
                templateUrl: 'partials/farmer/contactanos.html',
                data: {
                    css: ['css/contactanos.css']
                }
            })

            .state('farmer.charts', {
                url: '/charts',
                templateUrl: 'partials/merchant/charts.html',
                data: {
                    requireAuthorization: true
                },
                controller: 'chartscontroller',
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['js/Chart.js', 'js/pieDummyData.js']
                        })
                    }
                }
            })

            .state('farmer.profile', {
                url: '/profile/:userid',
                templateUrl: 'partials/farmer/profile.html',
                controller: 'profilecontroller',
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/profilecontroller.js']
                        });
                    }
                }
            })

            .state('merchant.categoria', {
                url: '/categoria',
                templateUrl: 'partials/merchant/categoria.html',
                data: {
                    css: ['css/estilo.css'],
                    requireAuthorization: true

                }
            })

            .state('merchant.categoriaResult', {
                url: '/categoria/Resultado',
                templateUrl: 'partials/merchant/categoriaResult.html',
                data: {
                    css: ['css/estilo.css'],
                    requireAuthorization: true
                }
            })

            .state('merchant.searchResult', {
                url: '/busqueda',
                templateUrl: 'partials/merchant/search.html',
                data: {
                    css: ['css/estilo.css'],
                    requireAuthorization: true
                }
            })


            .state('merchant', {
                url: '/merchant',
                templateUrl: 'partials/merchant/merchant.html',
                data: {
                    css: ['css/estilo.css'],
                    requireAuthorization: true
                },
                redirectTo: 'merchant.offers'
            })

            .state('merchant.offers', {
                url: '/offers',
                templateUrl: 'partials/merchant/offers.html',
                controller: 'offersformerchantcontroller',
                data: {
                    requireAuthorization: true
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/offersformerchantcontroller.js']
                        });
                    }
                }
            })

            .state('merchant.neworder', {
                url: '/neworder',
                templateUrl: 'partials/merchant/neworder.html',
                controller: 'newordercontroller',
                data: {
                    requireAuthorization: true
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/newordercontroller.js']
                        });
                    }
                }
            })

            .state('merchant.orders', {
                url: '/orders',
                templateUrl: 'partials/merchant/orders.html',
                controller: 'merchantorderscontroller',
                data: {
                    requireAuthorization: true
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/merchantorderscontroller.js']
                        });
                    }
                }
            })

            .state('merchant.about', {
                url: '/nosotros',
                templateUrl: 'partials/merchant/about.html',
                data: {
                    css: ['css/nosotros.css']
                }
            })

            .state('merchant.contactanos', {
                url: '/contactanos',
                templateUrl: 'partials/merchant/contactanos.html',
                data: {
                    css: ['css/contactanos.css']
                }
            })

            .state('merchat.charts', {
                url: '/charts',
                templateUrl: 'partials/merchant/charts.html',
                controller: 'chartscontroller',
                data: {
                    requireAuthorization: true
                },
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['js/Chart.js', 'js/pieDummyData.js']
                        });
                    }
                }
            })

            .state('merchant.profile', {
                url: '/profile/:userid',
                templateUrl: 'partials/merchant/profile.html',
                controller: 'profilecontroller',
                resolve: {
                    loadMyDirectives: function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'agroverde',
                            files: ['scripts/controllers/profilecontroller.js']
                        });
                    }
                }
            });

    })

    .factory('authService', function ($http, $state) {

        function authenticate(user, password, errorCallback) {
            var data = 'username=' + user + '&password=' + password + '&grant_type=password';

            $http.post('/Token', data, {
                'content-type': 'x-www-form-urlencoded'
            }).success(function (res) {

                if (res.access_token) {
                    localStorage.setItem('token', res.access_token);
                    isAuthenticated();
                    return true;

                } else {
                    $state.go('login');
                    return false;
                }

            }).error(function (err) {
                if (errorCallback && typeof errorCallback == 'function') {
                    errorCallback(err);
                } else {
                    $state.go('login');
                }
                localStorage.setItem('token', null);
                return false;
            });
        }

        function isAuthenticated() {
            var _token = localStorage.getItem('token');
            var auth_header = { 'Authorization': 'Bearer ' + _token };

            $http.get('api/Account/UserInfo', {
                headers: auth_header
            }).success(function (res) {

                if (res.Email) {
                    localStorage.setItem('user_info', JSON.stringify(res));
                    console.log(res);
                    if ($state.$current.name == 'login' || $state.$current.name == 'sign-up' || $state.$current.name == 'nosotros') {
                        //ir a una ruta dependiendo el rol del usuario
                        if (res.RoleName == 'Farmer') {
                            $state.go('farmer');
                        }

                        if (res.RoleName == 'Merchant') {
                            $state.go('merchant');
                        }

                    }

                    return true;

                } else {
                    localStorage.setItem('token', null);
                    localStorage.setItem('user_info', null);
                    if ($state.$current.name != 'sign-up' || $state.$current.name != 'nosotros') {
                        $state.go('login');
                    }

                    return false;
                }

            }).error(function (err) {
                localStorage.setItem('token', null);
                localStorage.setItem('user_info', null);
                if ($state.$current.name != 'sign-up') {
                    $state.go('login');
                }
                return false;
            });
        }

        function logOut() {
            localStorage.setItem('token', null);
            localStorage.setItem('user_info', null);
            isAuthenticated();
        }

        return {
            'authenticate': authenticate,
            'isAuthenticated': isAuthenticated,
            'logOut': logOut
        }

    })

    .run(['$rootScope', '$location', '$state', 'authService',
        function ($rootScope, $location, $state, authService) {
            $rootScope.$on('$stateChangeStart', function (e, toState, toParams
                , fromState, fromParams) {
                if (toState.redirectTo) {
                    e.preventDefault();
                    $state.go(toState.redirectTo, toParams, { location: 'replace' })
                }
                if (toState.data.requireAuthorization) {
                    authService.isAuthenticated();
                }
            });
        }]);
