﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AgroverdeApi.Models
{
    public class ProductSalesQuotesDetail
    {
        public int Id { get; set; }
        [ForeignKey("ProductSalesQuote")]
        public int ProductSalesQuoteId { get; set; }
        [ForeignKey("Measurement")]
        public int MeasurementId { get; set; }
        public decimal Quantity { get; set; }
        public decimal MeasurementPrice { get; set; }
        public decimal Total
        {
            get
            {
                return Quantity * MeasurementPrice;
            }
        }
        public virtual Measurement Measurement { get; set; }
        public virtual ProductSalesQuote ProductSalesQuote { get; set; }
    }
}