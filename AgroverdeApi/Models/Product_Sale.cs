namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product_Sale
    {
        public Product_Sale()
        {
            ProductSaleMeasuramentDetail = new List<Models.ProductSaleMeasuramentDetail>();
        }
        public int Id { get; set; }
        [Column("Product_id")]
        public int? Product_id { get; set; }

        [StringLength(128)]
        public string Owners { get; set; }

        [NotMapped]
        public string Photo { get; set; }

        [NotMapped]
        public string PhotoBase64 { get {

                if (Images != null && Images.Length > 0)
                {
                    return "data:image/jpeg;base64," + Convert.ToBase64String(Images);
                }
                else {
                    return Product?.Photo;
                }

            } }

        public byte[] Images { get; set; } = default(byte[]);

        public bool Publish { get; set; } = true;

        [StringLength(800)]
        public string Details { get; set; }

        public DateTime Date { get; set; }

        [ForeignKey("Product_id")]
        public virtual Product Product { get; set; }

        public virtual List<ProductSaleMeasuramentDetail> ProductSaleMeasuramentDetail { get; set; }

    }

    public class ProductSaleMeasuramentDetail
    {
        public int Id { get; set; }
        [ForeignKey("ProductSale")]
        public int ProductSaleId { get; set; }
        [ForeignKey("Measurement")]
        public int MeasuramentId { get; set; }
        public decimal Price { get; set; }
        public virtual Product_Sale ProductSale { get; set; }
        public virtual Measurement Measurement { get; set; }
    }

    public class Product_SaleViewModel
    {
        public Product_Sale Product_Sale { get; set; }

        public Personal_Information Owner { get; set; }
    }
}
