﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace AgroverdeApi.Models
{
    public class ProductSalesQuote
    {
        public ProductSalesQuote()
        {
            ProductSalesQuotesDetails = new List<ProductSalesQuotesDetail>();
            FarmerMerchantComments = new List<FarmerMerchantComment>();
        }
        public int Id { get; set; }
        [ForeignKey("ProductSale")]
        public int ProductSaleId { get; set; }
        [ForeignKey("Merchant")]
        public string MerchantId { get; set; }
        [ForeignKey("Farmer")]
        public string FarmerId { get; set; }
        public DateTime QuoteDate { get; set; }
        public DateTime QuoteStatusDateChange { get; set; }
        public QuoteStatus QuoteStatus { get; set; }
        public decimal? SubTotal { get {
                return ProductSalesQuotesDetails?.Sum(x => x.Total);
            }
        }
        //El total es la suma del subtotal mas tasa
        public decimal Total { get {
                return Tax + (SubTotal ?? 0m) ;
            }
        }
        //Calcular Tasa del banco  mas  Tasa de nosotros
        public decimal Tax { get; set; }
        public virtual Product_Sale ProductSale { get; set; }
        public virtual ApplicationUser Merchant { get; set; }
        public virtual ApplicationUser Farmer { get; set; }
        public virtual List<ProductSalesQuotesDetail> ProductSalesQuotesDetails { get; set; }
        public virtual List<FarmerMerchantComment> FarmerMerchantComments { get; set; }
    }

    public enum QuoteStatus
    {
        NotSelected,
        Aprobada,
        Rechazada,
        Procesando
    }
}