namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Personal_Information
    {
        public Personal_Information()
        {
            MunicipioUser = null;
        }

        public int Id{ get; set; }
        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(40)]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName { get { return this.Name + " " + this.LastName; } }

        [StringLength(13)]
        public string Identification { get; set; }

        public int? Municipio { get; set; }


        [Required]
        [StringLength(128)]
        public string UserId { get; set; }


        [StringLength(13)]
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        #region NotMappedAttributes
        [NotMapped]
        public string MunicipioName { get { return this.MunicipioUser?.Nombre; } }
        [NotMapped]

        public string ProvinciaName { get { return MunicipioUser?.Provincia?.Nombre; } }
        [ForeignKey("Municipio")]
        public virtual Municipio MunicipioUser { get; set; }

        #endregion
    }
}
