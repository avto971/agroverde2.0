﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgroverdeApi.Models
{
    public class ProductSalesTransaction
    {
        public int Id { get; set; }

        public DateTime TransactionDate { get; set; }

        public int TransactionConfirmationNumber { get; set; }

        //public string TransactionComment { get; set; }

        public int MyProperty { get; set; }
    }
}