namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product_Category
    {
       public Product_Category()
        {
            
        }

        public int Id { get; set; }

        [StringLength(45)]
        public string Category { get; set; }

        public string ImageCategory { get; set; }
    }
}
