﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System;

namespace AgroverdeApi.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        //public  string Id { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : base(name) { }
        public string Description { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("AgroverdeDbContext", throwIfV1Schema: false)
        {
        }


        public virtual DbSet<Product_Request> Product_Request { get; set; }
        public virtual DbSet<ProductSaleMeasuramentDetail> ProductSaleMeasuramentDetails { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Measurement> Measurements { get; set; }
        public virtual DbSet<Municipio> Municipios { get; set; }
        public virtual DbSet<Personal_Information> Personal_Information { get; set; }
        public virtual DbSet<Phone> Phones { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Product_Category> Product_Category { get; set; }
        public virtual DbSet<Product_Sale> Product_Sale { get; set; }
        public virtual DbSet<Provincia> Provincias { get; set; }
        public virtual DbSet<User_Prod_Cat> User_Prod_Cat { get; set; }
        public virtual DbSet<ProductSalesQuote> ProductSalesQuotes { get; set; }
        public virtual DbSet<FarmerMerchantComment> FarmerMerchantComments { get; set; }

        
        public virtual DbSet<ProductSalesTransaction> ProductSalesTransactions { get; set; }
        public virtual DbSet<ProductSalesQuotesDetail> ProductSalesQuotesDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<IdentityUser>().ToTable("Usuario").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<ApplicationUser>().ToTable("Usuario").Property(p => p.Id).HasColumnName("UserId");
            modelBuilder.Entity<IdentityUserRole>().ToTable("RolUsuario");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UsuarioLogeado");
            modelBuilder.Entity<IdentityRole>().ToTable("Rol");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Comment>()
                .Property(e => e.Comment1)
                .IsUnicode(false);

            modelBuilder.Entity<Measurement>()
                .Property(e => e.Detail)
                .IsUnicode(false);



            modelBuilder.Entity<Municipio>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Personal_Information>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Personal_Information>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<Personal_Information>()
                .Property(e => e.Identification)
                .IsUnicode(false);

            modelBuilder.Entity<Personal_Information>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Phone>()
                .Property(e => e.PhoneNum)
                .IsUnicode(false);

            modelBuilder.Entity<Phone>()
                .Property(e => e.Cedula)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Photo)
                .IsUnicode(false);


            modelBuilder.Entity<Product_Category>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<Provincia>()
                .Property(e => e.Nombre)
                .IsUnicode(false);

            modelBuilder.Entity<Product_Request>()
                .Property(e => e.Details)
                .IsUnicode(false);
        }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}