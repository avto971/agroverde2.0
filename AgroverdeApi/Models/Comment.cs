namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Comment")]
    public partial class Comment
    {
        public int Id { get; set; }

        [Column("Comment")]
        [StringLength(144)]
        public string Comment1 { get; set; }

        public int? Product_Sale { get; set; }
    }
}
