﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgroverdeApi.Models
{
    public class ProductSalesQuoteViewModel
    {
        public ProductSalesQuoteViewModel()
        {
            ProductSalesQuotesDetails = new List<ProductSalesQuotesDetail>();
            FarmerMerchantComments = new List<FarmerMerchantComment>();
        }
        public int Id { get; set; }
        public int ProductSaleId { get; set; }
        public string ProductSaleName { get; set; }
        public string MerchantId { get; set; }
        public string MerchantName{ get; set; }
        public string FarmerId { get; set; }
        public string FarmerName{ get; set; }
        public DateTime QuoteDate { get; set; }
        public DateTime QuoteStatusDateChange { get; set; }
        public string QuoteStatus { get; set; }
        public decimal? SubTotal {get;set;}
        //El total es la suma del subtotal mas tasa
        public decimal Total  {get;set; }
        public decimal Tax { get; set; }
        public  List<ProductSalesQuotesDetail> ProductSalesQuotesDetails { get; set; }
        public  List<FarmerMerchantComment> FarmerMerchantComments { get; set; }

    }
}