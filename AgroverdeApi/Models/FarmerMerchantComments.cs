﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AgroverdeApi.Models
{
    public class FarmerMerchantComment
    {
        public int Id { get; set; }
        [ForeignKey("ProductSalesQuote")]
        public int? ProductQuoteSaleId { get; set; }
        [ForeignKey("Farmer")]
        public string FarmerId { get; set; }
        [ForeignKey("Merchant")]
        public string MerchantId { get; set; }
        public string Comment { get; set; }
        //Esta propiedad es para saber si el responsable del comentario es el Merchant
        public bool IsMerchantComment { get; set; }
        public DateTime CommentDate { get; set; }
        public virtual ApplicationUser Farmer { get; set; }
        public virtual ApplicationUser Merchant { get; set; }
        public virtual ProductSalesQuote ProductSalesQuote { get; set; }   
    }
}