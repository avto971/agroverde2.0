﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using AgroverdeApi.Services.Interfaces;

namespace AgroverdeApi.Services.Implementation
{

    public class ConfiguracionCorreo 
    {
        public string Email { get; set; }
        public string Nombre { get; set; }
        public string Asunto { get; set; }
        public string Descripcion { get; set; }
    }


    public class EmailSenderService : IEmailSenderService
    {
        public MailMessage message;
        public readonly string templatesEmailPath;


        public EmailSenderService()
        {
            message = new MailMessage();
            templatesEmailPath = HttpContext.Current.Server.MapPath(System.Web.Configuration.WebConfigurationManager.AppSettings["templateEmailPath"]);

        }


        public void Add(string to)
        {
            message.To.Add(to);
        }


        public void Add(ICollection<string> to)
        {
            foreach (var address in to)
            {
                message.To.Add(address);
            }
        }

        public void SetConfiguration(ConfiguracionCorreo model, string template = "")
        {
            var path = HttpContext.Current.Server.MapPath(WebConfigurationManager.AppSettings["templateEmailPath"]);
            // var HtmlView = ReadTemplateFile("Email.html", path);
            var HtmlView = ReadTemplateFile("template-email.html", path);
            ReplaceTokens(ref HtmlView, model, template);

            message.Subject = model.Asunto;
            message.Body = HtmlView;
            message.IsBodyHtml = true;
        }

        public virtual string ReadTemplateFile(string filename, string templatePath = null)
        {
            templatePath = templatePath ?? templatesEmailPath;
            string textfilePath = Path.Combine(templatePath, filename);
            string culturePath = Path.Combine(templatePath, CultureInfo.CurrentUICulture.Name);

            // If there's templates for the current culture, then use those instead
            if (!Directory.Exists(culturePath)) return File.ReadAllText(textfilePath);
            var culturePlainTextFile = Path.Combine(culturePath, filename);
            if (File.Exists(culturePlainTextFile))
                textfilePath = culturePlainTextFile;

            return File.ReadAllText(textfilePath);
        }

        public virtual void ReplaceTokens(ref string layout, ConfiguracionCorreo model, string template)
        {
            layout = layout.Replace("{NOMBRE}", model.Nombre);
            layout = layout.Replace("{DESCRIPCION}", model.Descripcion);
            layout = layout.Replace("{EMAIL}", model.Email);
        }


        public async Task SendMessage()
        {
            try
            {
                using (var client = new SmtpClient())
                {
                    client.Timeout = 120;
                    await client.SendMailAsync(message);
                }

            }catch(Exception ex)
            {
            }

        }

    }
}