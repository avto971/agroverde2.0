﻿using AgroverdeApi.Services.Implementation;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace AgroverdeApi.Services.Interfaces
{
    public interface IEmailSenderService
    {
        void SetConfiguration(ConfiguracionCorreo model, string template = "");
        void Add(ICollection<string> to);
        void Add(string to);
        Task SendMessage();
    }
}
