﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AgroverdeApi.Models;

namespace AgroverdeApi.Controllers
{
    public class Product_RequestController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [ResponseType(typeof(Product_RequestViewModel))]
        // GET: api/Product_Request
        public IHttpActionResult GetProduct_Request()
        {

      
            var requests = new List<Product_RequestViewModel>();

            foreach (var p in db.Product_Request.AsEnumerable())
            {
                requests.Add(new Product_RequestViewModel
                {
                    Product_Request = p,
                    Requester = db.Personal_Information.Single(u => u.UserId == p.Requester)
                });
            }

            return Ok(requests);
        }



        // GET: api/Product_Request/5
        [ResponseType(typeof(Product_Request))]
        public IHttpActionResult GetProduct_Request(int id)
        {
            Product_Request product_Request = db.Product_Request.Find(id);
            if (product_Request == null)
            {
                return NotFound();
            }

            return Ok(product_Request);
        }

        [ResponseType(typeof(Product_Sale))]
        [Route("api/product_request/userid/{userid}")]
        public IHttpActionResult GetProduct_RequestByUserId(string userid)
        {
            var products = from p in db.Product_Request
                           where p.Requester == userid
                           select p;
            if (products.FirstOrDefault() == null) return NotFound();

            return Ok(products);
        }

        // PUT: api/Product_Request/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct_Request(int id, Product_Request product_Request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product_Request.Id)
            {
                return BadRequest();
            }

            db.Entry(product_Request).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Product_RequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Product_Request
        [ResponseType(typeof(Product_Request))]
        public IHttpActionResult PostProduct_Request(Product_Request product_Request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Product_Request.Add(product_Request);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (Product_RequestExists(product_Request.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = product_Request.Id }, product_Request);
        }

        // DELETE: api/Product_Request/5
        [ResponseType(typeof(Product_Request))]
        public IHttpActionResult DeleteProduct_Request(int id)
        {
            Product_Request product_Request = db.Product_Request.Find(id);
            if (product_Request == null)
            {
                return NotFound();
            }

            db.Product_Request.Remove(product_Request);
            db.SaveChanges();

            return Ok(product_Request);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Product_RequestExists(int id)
        {
            return db.Product_Request.Count(e => e.Id == id) > 0;
        }
    }
}