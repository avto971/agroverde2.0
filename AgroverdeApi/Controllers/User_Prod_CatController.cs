﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AgroverdeApi.Models;

namespace AgroverdeApi.Controllers
{
    public class User_Prod_CatController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/User_Prod_Cat
        public IQueryable<User_Prod_Cat> GetUser_Prod_Cat()
        {
            return db.User_Prod_Cat;
        }

        // GET: api/User_Prod_Cat/5
        [ResponseType(typeof(User_Prod_Cat))]
        public IHttpActionResult GetUser_Prod_Cat(string id)
        {
            User_Prod_Cat user_Prod_Cat = db.User_Prod_Cat.Find(id);
            if (user_Prod_Cat == null)
            {
                return NotFound();
            }

            return Ok(user_Prod_Cat);
        }

        // PUT: api/User_Prod_Cat/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser_Prod_Cat(string id, User_Prod_Cat user_Prod_Cat)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user_Prod_Cat.UserID)
            {
                return BadRequest();
            }

            db.Entry(user_Prod_Cat).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!User_Prod_CatExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/User_Prod_Cat
        [ResponseType(typeof(User_Prod_Cat))]
        public IHttpActionResult PostUser_Prod_Cat(User_Prod_Cat user_Prod_Cat)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.User_Prod_Cat.Add(user_Prod_Cat);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (User_Prod_CatExists(user_Prod_Cat.UserID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = user_Prod_Cat.UserID }, user_Prod_Cat);
        }

        // DELETE: api/User_Prod_Cat/5
        [ResponseType(typeof(User_Prod_Cat))]
        public IHttpActionResult DeleteUser_Prod_Cat(string id)
        {
            User_Prod_Cat user_Prod_Cat = db.User_Prod_Cat.Find(id);
            if (user_Prod_Cat == null)
            {
                return NotFound();
            }

            db.User_Prod_Cat.Remove(user_Prod_Cat);
            db.SaveChanges();

            return Ok(user_Prod_Cat);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool User_Prod_CatExists(string id)
        {
            return db.User_Prod_Cat.Count(e => e.UserID == id) > 0;
        }
    }
}