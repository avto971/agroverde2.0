﻿using AgroverdeApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace AgroverdeApi.Controllers
{
    [RoutePrefix("api/PersonalInformation")]
    public class PersonalInformationController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/PersonalInformation
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/PersonalInformation/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PersonalInformation
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/PersonalInformation/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PersonalInformation/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        [Route("GetPersonalInformationFromUser/{userId}")]
        [ResponseType(typeof(Personal_Information))]
        public IHttpActionResult GetPersonalInformationFromUser(string userId) {
            var personalInfo = db.Personal_Information.FirstOrDefault(x => x.UserId == userId);
            return Ok(personalInfo);
        }
    }
}
