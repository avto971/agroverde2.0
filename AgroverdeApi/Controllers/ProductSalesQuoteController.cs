﻿using AgroverdeApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
namespace AgroverdeApi.Controllers
{
    [RoutePrefix("api/ProductSalesQuotes")]
    public class ProductSalesQuotesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ProductSalesQuote
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ProductSalesQuote/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ProductSalesQuote
        public IHttpActionResult Post([FromBody]ProductSalesQuote value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            var added = db.ProductSalesQuotes.Add(value);
            db.SaveChanges();
            return Ok(added);
        }

        // PUT: api/ProductSalesQuote/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ProductSalesQuote/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        [Route("GetQuotes/")]
        [ResponseType(typeof(ProductSalesQuoteViewModel))]
        public IHttpActionResult GetQuotes([FromUri]string farmerId  = null, [FromUri] string merchantId = null, [FromUri] QuoteStatus quoteStatus = QuoteStatus.Procesando)
        {
            var productQuotes = new List<ProductSalesQuoteViewModel>();

            var quotes = db.ProductSalesQuotes.Where(x =>
            (!string.IsNullOrEmpty(farmerId) ?  (x.FarmerId == farmerId) : true) && (!string.IsNullOrEmpty(merchantId) ? (x.MerchantId == merchantId) : true)
            && x.QuoteStatus == quoteStatus).ToList();

            foreach (var quote in quotes)
            {
                var farmer = db.Personal_Information.FirstOrDefault(x => x.UserId == quote.FarmerId);
                var farmerName = farmer.FullName;
                var merchant = db.Personal_Information.FirstOrDefault(x => x.UserId == quote.MerchantId);
                var merchantName = merchant.FullName;
                productQuotes.Add(new ProductSalesQuoteViewModel
                {
                    MerchantId = quote.MerchantId,
                    FarmerId = quote.FarmerId,
                    FarmerMerchantComments= quote.FarmerMerchantComments,
                    ProductSalesQuotesDetails = quote.ProductSalesQuotesDetails,
                    QuoteDate  = quote.QuoteDate,
                    QuoteStatus = quote.QuoteStatus.ToString(),
                    QuoteStatusDateChange = quote.QuoteStatusDateChange,
                    ProductSaleId = quote.ProductSaleId,
                    SubTotal = quote.SubTotal,
                    Tax = quote.Tax,
                    Id  = quote.Id,
                    FarmerName = farmerName,
                    MerchantName = merchantName,
                    ProductSaleName = quote.ProductSale.Product.Name
                });
            }

            return Ok(productQuotes);
        }

        [HttpPost]
        [Route("SaveComment")]
        [ResponseType(typeof(FarmerMerchantComment))]
        public IHttpActionResult SaveComment([FromBody]FarmerMerchantComment comment)
        {
            comment.CommentDate = DateTime.Now;
            db.FarmerMerchantComments.Add(comment);
            db.SaveChanges();

            return Ok(comment);

        }

    }
}
