﻿using AgroverdeApi.Services.Implementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace AgroverdeApi.Controllers
{
    public class ManageController : ApiController
    {
        // GET: Manage
        [Route("api/Manage/SendEmail")]
        [HttpPost]
        [AllowAnonymous]
        public async System.Threading.Tasks.Task<IHttpActionResult> SendEmailAsync(ConfiguracionCorreo model)
        {
            var emailsender = new EmailSenderService();
            var emailAgroverde = WebConfigurationManager.AppSettings["EmailAgroverde"];
            emailsender.Add(emailAgroverde);
            emailsender.SetConfiguration(model);
            await emailsender.SendMessage();

            return Ok(new { state = true });
        }
    }
}