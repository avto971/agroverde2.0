﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AgroverdeApi.Models;

namespace AgroverdeApi.Controllers
{
    public class ProvinciasController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Provincias
        public IQueryable<Provincia> GetProvincias()
        {
            return db.Provincias;
        }

        // GET: api/Provincias/5
        [ResponseType(typeof(Provincia))]
        public IHttpActionResult GetProvincia(int id)
        {
            Provincia provincia = db.Provincias.Find(id);
            if (provincia == null)
            {
                return NotFound();
            }

            return Ok(provincia);
        }

    }
}