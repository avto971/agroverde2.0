﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AgroverdeApi.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace AgroverdeApi.Controllers
{
    public class Product_SaleController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationDbContext applicationDbContext = new ApplicationDbContext();

        [ResponseType(typeof(Product_SaleViewModel))]
        // GET: api/Product_Sale
        public IHttpActionResult GetProduct_Sale()
        {
            var products = new List<Product_SaleViewModel>();
            var productSales = db.Product_Sale.AsEnumerable();
            foreach (var p in productSales)
            {
                products.Add(new Product_SaleViewModel
                {
                    Product_Sale = p,
                    Owner = db.Personal_Information.FirstOrDefault(pi => pi.UserId == p.Owners)
                });
            }

            return Ok(products);
        }
        [HttpGet]
        [Route("api/ProductSale/GetProductSaleDetail/{id}")]
        [ResponseType(typeof(Product_SaleViewModel))]
        public async  Task<IHttpActionResult> GetProductSaleDetail(int id)
        {
            var  product_Sale = await db.Product_Sale
                .Include("Product")
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
            var   personal_information = await db
                .Personal_Information
                .FirstOrDefaultAsync(x => x.UserId == product_Sale.Owners);

            var response = new Product_SaleViewModel()
            {
                Product_Sale = product_Sale,
                Owner = personal_information
            };

            var category = await (from c in db.Product_Category
                           where c.Id == product_Sale.Product.CategoryId
                           select c).FirstOrDefaultAsync();


            response.Product_Sale.Product.CategoryName = category.Category;
            //response.Owner.MunicipioName = municipio.Nombre;
            //response.Owner.ProvinciaName = provincia.Nombre;

            //var product_Sale = db.Product_Sale.Include("Product").Where(x => x.Product.CategoryId == id).ToList();
            //var products = new List<object>();

            //products.AddRange(product_Request);
            //products.AddRange(product_Sale);
            if (product_Sale == null)
            {
                return NotFound();
            }
            return Ok(response);
        }

        [Route("api/product_sale/userid/{user_id}")]
        [ResponseType(typeof(Product_Sale))]
        public IHttpActionResult GetProduct_SaleByUserId(string user_id)
        {
            var products = db.Product_Sale.Where(x => x.Owners == user_id).OrderBy(x=> x.Date);

            if (products.FirstOrDefault() == null) return NotFound();
            else
            return Ok(products.AsEnumerable());
        }

        // GET: api/Product_Sale/5
        [ResponseType(typeof(Product_Sale))]
        public IHttpActionResult GetProduct_Sale(int id)
        {
            Product_Sale product_Sale = db.Product_Sale.Find(id);
            if (product_Sale == null)
            {
                return NotFound();
            }

            return Ok(product_Sale);
        }

        [HttpGet]
        [Route("api/product_sale/GetProduct_SaleMeasurementDetails/{id}")]
        [ResponseType(typeof(List<ProductSaleMeasuramentDetail>))]
        public IHttpActionResult GetProduct_SaleMeasurementDetails(int id)
        {
            var products = db.Product_Sale.FirstOrDefault(x => x.Id == id).ProductSaleMeasuramentDetail;

            if (products.FirstOrDefault() == null) return NotFound();
            else
                return Ok(products);
        }


        [HttpPost]
        [Route("api/product_sale/updateProductSale")]
        [ResponseType(typeof(void))]
        public IHttpActionResult UpdateProductSale(Product_Sale product_Sale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Entry(product_Sale).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Product_SaleExists(product_Sale.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Product_Sale
        [ResponseType(typeof(Product_Sale))]
        public IHttpActionResult PostProduct_Sale(Product_Sale product_Sale)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                product_Sale.Images = Convert.FromBase64String(product_Sale.Photo.Replace("data:image/jpeg;base64,", ""));
            } catch (Exception) {}

            product_Sale.Date =  DateTime.Now;

            db.Product_Sale.Add(product_Sale);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (Product_SaleExists(product_Sale.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = product_Sale.Id }, product_Sale);
        }

        // DELETE: api/Product_Sale/5
        [ResponseType(typeof(Product_Sale))]
        public IHttpActionResult DeleteProduct_Sale(int id)
        {
            Product_Sale product_Sale = db.Product_Sale.Find(id);
            if (product_Sale == null)
            {
                return NotFound();
            }

            db.Product_Sale.Remove(product_Sale);
            db.SaveChanges();

            return Ok(product_Sale);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Product_SaleExists(int id)
        {
            return db.Product_Sale.Count(e => e.Id == id) > 0;
        }


    }
}