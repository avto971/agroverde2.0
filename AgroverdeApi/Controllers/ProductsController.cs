﻿    using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AgroverdeApi.Models;

namespace AgroverdeApi.Controllers
{
    public class ProductsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Products
        public IQueryable<Product> GetProducts()
        {
            return db.Products;
        }

        // GET: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetProduct(int id)
        {
            var products = from p in db.Products
                           where p.CategoryId == id
                           select p;

            if (products.FirstOrDefault() == null)
            {
                return NotFound();
            }

            return Ok(products);
        }

        [HttpGet]
        [Route("api/Products/FilterProductByCategoryId/{id}")]
        [ResponseType(typeof(Product))]
        public IHttpActionResult FilterProductByCategoryId(int id)
        {
            var product_Request = db.Product_Request.Include("Product").Where(x => x.Product.CategoryId == id).ToList();
            var product_Sale = db.Product_Sale.Include("Product").Where(x => x.Product.CategoryId == id).ToList();
            var products = new List<object>();

            products.AddRange(product_Request);
            products.AddRange(product_Sale);

            if (products.FirstOrDefault() == null)
            {
                return NotFound();
            }

            return Ok(products);
        }



        //[HttpGet]
        //[Route("api/Products/GetProductSaleDetail/{id}")]
        //[ResponseType(typeof(Product_Sale))]
        //public IHttpActionResult GetProductSaleDetail(int id)
        //{
        //    var product_Sale = db.Product_Sale
        //        .Include("Product")
        //        .Include("Measurement")
        //        .Where(x => x.Id == id)
        //        .FirstOrDefault();
        //    //var product_Sale = db.Product_Sale.Include("Product").Where(x => x.Product.CategoryId == id).ToList();
        //    //var products = new List<object>();

        //    //products.AddRange(product_Request);
        //    //products.AddRange(product_Sale);
        //    if (product_Sale == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(product_Sale);
        //}

        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.Id)
            {
                return BadRequest();
            }

            db.Entry(product).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Products
        [ResponseType(typeof(Product))]
        public IHttpActionResult PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Products.Add(product);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = product.Id }, product);
        }

        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult DeleteProduct(int id)
        {
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            db.Products.Remove(product);
            db.SaveChanges();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Products.Count(e => e.Id == id) > 0;
        }
    }
}