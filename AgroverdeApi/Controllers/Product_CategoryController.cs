﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AgroverdeApi.Models;

namespace AgroverdeApi.Controllers
{
    public class Product_CategoryController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Product_Category
        public IQueryable<Product_Category> GetProduct_Category()
        {
            return db.Product_Category;
        }

        // GET: api/Product_Category/5
        [ResponseType(typeof(Product_Category))]
        [Route("api/ProductCategory/GetProduct_Category/{id}")]
        public IHttpActionResult GetProduct_Category(int id)
        {
            Product_Category product_Category = db.Product_Category.Find(id);
            if (product_Category == null)
            {
                return NotFound();
            }

            return Ok(product_Category);
        }

       
        // GET: api/Product_Category/5
        [ResponseType(typeof(Product_Category))]
        public IHttpActionResult GetProduct_ByCategory(int CategoryId)
        {
            //Producto_Sale and Product_request
            var products = db.Product_Sale.Include(x=> x.Product).Where(x => x.Product.CategoryId == CategoryId);

            return Ok(products);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Product_CategoryExists(int id)
        {
            //  return db.Product_Category.Count(e => e.Id == id) > 0;
              return db.Product_Category.Any(e => e.Id == id);
        }
    }
}