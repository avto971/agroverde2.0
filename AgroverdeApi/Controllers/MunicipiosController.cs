﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AgroverdeApi.Models;

namespace AgroverdeApi.Controllers
{
    public class MunicipiosController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Municipios
        public IEnumerable<Municipio> GetMunicipios()
        {
            return db.Municipios;
        }

        // GET: api/Municipios/5
        [ResponseType(typeof(Municipio))]
        public IHttpActionResult GetMunicipio(int id)
        {
            //var municipios = from m in db.Municipios
            //                 where m.Provincia_id == id
            //                 select m;

            var municipios = db.Municipios.Where(x => x.Provincia_id == id);

            if (municipios.FirstOrDefault() == null)
            {
                return NotFound();
            }

            return Ok(municipios.AsEnumerable());
        }
    }
}