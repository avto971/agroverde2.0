﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Agroverde.Admin.Startup))]
namespace Agroverde.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
