﻿var BaseApi = function (baseuri) {
    this.baseUri = baseuri;

    this.getAll = function (callback) {
        this.ajax("GET", this.baseUri + "/List", {}, callback);
    }
    this.add = function(data, callback) {
        this.ajax("POST", this.baseUri + "/Create", JSON.stringify(data), callback);
    }

    this.getById = function (id, callback) {
        this.ajax("GET", this.baseUri + "/Details/"+ id, JSON.stringify(data), callback);
    }

    this.update = function (data, callback) {
        this.ajax("POST", this.baseUri + "/Edit", JSON.stringify(data), callback);
    }

    this.delete = function (id, callback) {
        this.ajax("POST", this.baseUri + "/Delete/" +  id, null, callback);
    }
}

BaseApi.prototype.ajax = function(type, url, data, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open(type, url);
    xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    xhr.withCredentials = false;
    xhr.onload = function () {

        if (xhr.status != 200) {
            alert('HTTP Error: ' + xhr.status);
            return;
        }

        var json = JSON.parse(xhr.responseText);

        //if (!json || typeof json.location != 'string') {
        //    failure('Invalid JSON: ' + xhr.responseText);
        //    return;
        //}

       // success(json.location);

        callback(json);
    };

    xhr.send(data);

}