﻿
var CategoriaVm = function () {
    var self = this;

    self.categorias = ko.observableArray([]);
    self.model = ko.validatedObservable(new CategoriaModel());
    self.api = new BaseApi("/Product_Category");
    self.table =null;
  
    self.cargar = function () {
        self.api.getAll((response) => {
            self.categorias(response);
        });
    }
    self.guardar = function() {
        if (self.model.isValid()) {
            if (self.model().Id()) {
                self.api.update(ko.toJS(self.model), function (response) {
                    console.log(response);

                    swal(
                        'Editado!',
                        'La categoria ha sido editada',
                        'success'
                    )

                    self.cargar();
                    self.cancel();

                });
            } else {
                self.api.add(ko.toJS(self.model), function (response) {
                    console.log(response);

                    swal(
                        'Guardado!',
                        'La categoria ha sido guardada',
                        'success'
                    )
                    self.cargar();
                    self.cancel();

                });
            }
            
        }
    }
    self.add =  () => {
        if (self.model().Id()) {
            self.cancel();
        }
    };
    self.loadCategoria = function (categoria) {
        ko.mapping.fromJS(ko.mapping.toJS(categoria), {}, self.model);
    }

    self.cancel  = function() {
        ko.mapping.fromJS(ko.mapping.toJS(new CategoriaModel()), {}, self.model);
        self.model.errors.showAllMessages(false);
         $(".modal").modal("hide")
    }

    self.borrarCategoria = function (categoria) {

        swal({
            title: 'Está seguro que desea eliminar la categoria',
            text: "No podrás revertir este cambio",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, borrar',
            cancelButtonText: 'No, Cancelar!',
            confirmButtonClass: 'btn btn-success fixer',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {
            self.api.delete(categoria.Id, function (response) {
                console.log(response);

                swal(
                    'Borrado!',
                    'La categoria ha sido Borrado',
                    'success'
                )
                self.cargar()
            });
        }, function (dismiss) {
            // dismiss can be 'cancel', 'overlay',
            // 'close', and 'timer'
            if (dismiss === 'cancel') {
                swal(
                    'Cancelado',
                    '',
                    'error'
                )
            }
        })


    }

}

ko.validation.init({ errorMessageClass: "validation-summary-errors" });
var obj = new CategoriaVm()
ko.applyBindings(obj);
obj.cargar();