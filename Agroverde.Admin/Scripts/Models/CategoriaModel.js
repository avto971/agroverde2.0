﻿var CategoriaModel = function () {
    var self = this;

    this.Id = ko.observable(null);
    this.Category = ko.observable(null).extend({ required: { params: true, message: "Campo obligatorio" } });
    this.InputFileImageCategory   = ko.observable(null).extend({ required: { params: true, message: "Campo obligatorio" } });
    this.ImageCategory = ko.observable(null);

    this.InputFileImageCategory.subscribe(data => {
        if (data) {
            var reader = new FileReader();
            reader.readAsDataURL(data);
            reader.onload = () => {
                var result = reader.result;
                var base64 = result.replace(/^data:image\/[a-z]+;base64,/, "");
                this.ImageCategory(base64);
            };
            reader.onerror = (error) => {
                this.ImageCategory("");
            };
        }
    });
}

