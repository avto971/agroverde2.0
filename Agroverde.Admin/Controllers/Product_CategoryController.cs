﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Agroverde.Admin.Models;
using AgroverdeApi.Models;

namespace Agroverde.Admin.Controllers
{
    public class Product_CategoryController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: Empleadoes
        public ActionResult Index()
        {
            return View();
        }

        // GET: Empleadoes
        public ActionResult List()
        {
            return Json(db.Product_Category.ToList(), JsonRequestBehavior.AllowGet);
        }

        // GET: Empleadoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product_Category product_Category = db.Product_Category.Find(id);
            if (product_Category == null)
            {
                return HttpNotFound();
            }
            return Json(product_Category, JsonRequestBehavior.AllowGet);
        }



        // POST: Empleadoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(Product_Category product_Category)
        {

            db.Product_Category.Add(product_Category);
            db.SaveChanges();
            return Json(product_Category, JsonRequestBehavior.AllowGet);
        }

        // POST: Empleadoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Edit(Product_Category product_Category)
        {
            db.Entry(product_Category).State = EntityState.Modified;
            db.SaveChanges();
            return Json(product_Category, JsonRequestBehavior.AllowGet);
        }

        // POST: Empleadoes/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            Product_Category empleado = db.Product_Category.Find(id);
            db.Product_Category.Remove(empleado);
            db.SaveChanges();
            return Json(empleado, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
