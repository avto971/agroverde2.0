namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public  class Personal_Information
    {
        [StringLength(40)]
        public string Name { get; set; }

        [StringLength(40)]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName { get {
                return this.Name + this.LastName;
            } }

        [Key]
        [StringLength(13)]
        public string Identification { get; set; }

        public int? Municipio { get; set; }

        [Required]
        [StringLength(128)]
        public string UserId { get; set; }


        [StringLength(13)]
        public string PhoneNumber { get; set; }
    }
}
