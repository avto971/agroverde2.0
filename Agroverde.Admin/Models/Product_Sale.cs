namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product_Sale
    {
        public int Id { get; set; }
        [Column("Product_id")]
        public int? Product_id { get; set; }

        [StringLength(128)]
        public string Owners { get; set; }

        [Column("Measurement_id")]
        public int? Measurement_id { get; set; }

        public int? Quantity { get; set; }

        public int? Price { get; set; }

        [StringLength(800)]
        public string Details { get; set; }

        public DateTime? Date { get; set; }
        [ForeignKey("Measurement_id")]
        public virtual Measurement Measurement { get; set; }
        [ForeignKey("Product_id")]
        public virtual Product Product { get; set; }

    }

    public class Product_SaleViewModel
    {
        public Product_Sale Product_Sale { get; set; }

        public Personal_Information Owner { get; set; }
    }
}
