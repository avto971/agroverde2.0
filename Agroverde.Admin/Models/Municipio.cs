namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Municipio")]
    public partial class Municipio
    {
        [Key]
        public int Municipio_id { get; set; }

        [StringLength(45)]
        public string Nombre { get; set; }

        public int? Provincia_id { get; set; }
        
    }
}
