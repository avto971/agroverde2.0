namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        public Product()
        {
            
        }

        public int Id { get; set; }

        [StringLength(45)]
        public string Name { get; set; }

        [Column("CategoryId")]
        public int? CategoryId { get; set; }

        [StringLength(500)]
        public string Photo { get; set; }
       
    }
}
