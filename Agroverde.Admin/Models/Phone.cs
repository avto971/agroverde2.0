namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Phone")]
    public partial class Phone
    {
        [Key]
        [StringLength(12)]
        public string PhoneNum { get; set; }

        [Required]
        [StringLength(13)]
        public string Cedula { get; set; }
    }
}
