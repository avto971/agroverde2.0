namespace AgroverdeApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product_Request
    {
       
        public int Id { get; set; }

        [Column("Product_id")]
        public int? Product_id { get; set; }

        [StringLength(128)]
        public string Requester { get; set; }

        [Column("Measurement_id")]
        public int? Measurement_id { get; set; }

        public int? Quantity { get; set; }

        public DateTime? Date { get; set; }

        [StringLength(800)]
        public string Details { get; set; }

        [ForeignKey("Product_id")]
        public virtual Product Product { get; set; }

        [ForeignKey("Measurement_id")]
        public virtual Measurement Measurement { get; set; }

    }

    public class Product_RequestViewModel
    {
        public Personal_Information Requester { get; set; }

        public Product_Request Product_Request { get; set; }
    }
}
