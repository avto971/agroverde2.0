namespace Agroverde.Admin.Migrations
{
    using Agroverde.Admin.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            AddUserAndRole(context);
        }

        bool AddUserAndRole(ApplicationDbContext context)
        {
            IdentityResult ir;
            var rm = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(context));

            ir = rm.Create(new ApplicationRole("Administrador") { Description = "Permite administrar el sistema en su totalidad." });
            ir = rm.Create(new ApplicationRole("Farmer") { Description = "Productor." });
            ir = rm.Create(new ApplicationRole("Merchant") { Description = "Cormeciante." });

            var um = new UserManager<ApplicationUser>(
                new UserStore<ApplicationUser>(context));
            var user = new ApplicationUser()
            {
                Email = "admin@agroverde.com",
                UserName = "admin@agroverde.com",
                EmailConfirmed = true
            };

            var user2 = new ApplicationUser()
            {
                Email = "farmer@agroverde.com",
                UserName = "farmer@agroverde.com",
                EmailConfirmed = true
            };

            var user3 = new ApplicationUser()
            {
                Email = "merchant@agroverde.com",
                UserName = "merchant@agroverde.com",
                EmailConfirmed = true
            };


            ir = um.Create(user, "Agroverde1234,");
            ir = um.Create(user2, "Agroverde1234,");
            ir = um.Create(user3, "Agroverde1234,");

            if (ir.Succeeded == false)
                return ir.Succeeded;
            ir = um.AddToRole(user.Id, "Administrador");
            ir = um.AddToRole(user2.Id, "Farmer");
            ir = um.AddToRole(user3.Id, "Merchant");

            return ir.Succeeded;
        }
    }
}
