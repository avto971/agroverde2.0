﻿Select * from Measurement;

insert into Measurement (Id, Detail)
Values (5, 'Unidades');

UPDATE Measurement SET Detail= 'Kilogramos' WHERE Id = 1;
UPDATE Measurement SET Detail= 'Libras' WHERE Id = 2;
UPDATE Measurement SET Detail= 'Onzas' WHERE Id = 3;
UPDATE Measurement SET Detail= 'Quintales' WHERE Id = 4;

Delete from Measurement where Id = 1;